#!/bin/bash

# Constantes
FAN_MIN_DUTY=0                #(%)
FAN_MED_DUTY=20               #(%)
FAN_MAX_DUTY=100              #(%)
FAN_DUTY_STEP_UDR_50=2        #(%)
FAN_DUTY_STEP_ABV_50=3        #(%)
FAN_ON_TIME=0.7               #(secondes)
FAN_OFF_TIME=0.3              #(secondes)
TEMPS=20                      #(secondes)



/usr/bin/sudo /usr/bin/echo 14 > /sys/class/gpio/unexport

if [ ! -d /sys/class/gpio/gpio14 ]; then
  /usr/bin/sudo /usr/bin/echo 14 > /sys/class/gpio/export
  /usr/bin/sudo /usr/bin/echo out > /sys/class/gpio/gpio14/direction
fi

fan_on() {
    /usr/bin/echo 1 | /usr/bin/sudo tee /sys/class/gpio/gpio14/value > /dev/null
}
fan_off() {
    /usr/bin/echo 0 | /usr/bin/sudo tee /sys/class/gpio/gpio14/value > /dev/null
}

duty=$FAN_MED_DUTY

while true; do
  cpu=$(</sys/class/thermal/thermal_zone0/temp)
  if [ $cpu -gt 50000 ] && [ $duty -lt $FAN_MAX_DUTY ]; then
    duty=$((($cpu / 1000 - 50) / 3 * 8 + 20))
    /usr/bin/echo "Pourcentage actuel $duty"
    /usr/bin/echo "Température actuelle: $(($cpu / 1000))"
    fan_on_time=$(/usr/bin/echo "$FAN_ON_TIME * $duty / 100" | bc -l | printf "%.0f")
    fan_off_time=$(/usr/bin/echo "$FAN_OFF_TIME * (100 - $duty) / 100" | bc -l | printf "%.0f")

  elif [ $cpu -lt 50000 ] && [ $duty -gt $FAN_MIN_DUTY ] && [ $cpu -gt 39999 ]; then
    duty=$((($cpu / 1000 - 40) * 2))
    /usr/bin/echo "Pourcentage actuel $duty"
    /usr/bin/echo "Température actuelle: $(($cpu / 1000))"
    fan_on_time=$(/usr/bin/echo "$FAN_ON_TIME * $duty / 100" | bc -l | printf "%.0f")
    fan_off_time=$(/usr/bin/echo "$FAN_OFF_TIME * (100 - $duty) / 100" | bc -l | printf "%.0f")
  elif [ $cpu -lt 40000 ]; then
    duty=0
    /usr/bin/echo "Pourcentage actuel $duty"
    /usr/bin/echo "Température actuelle: $(($cpu / 1000))"
    fan_on_time=$(/usr/bin/echo "$FAN_ON_TIME * $duty / 100" | bc -l | printf "%.0f")
    fan_off_time=$(/usr/bin/echo "$FAN_OFF_TIME * (100 - $duty) / 100" | bc -l | printf "%.0f")
  fi

  if [ -d /sys/class/gpio/gpio14 ]; then 
    start=$(/usr/bin/date +%s)
    while [ $(($(/usr/bin/date +%s) - $start)) -lt $TEMPS ]
    do
      fan_on
      sleep $fan_on_time
      fan_off
      sleep $fan_off_time
    done
  fi
done
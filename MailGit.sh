git filter-branch --env-filter '                                              
ANCIEN_EMAIL="oui@oui.fr"
NOUVEAU_PSEUDO="Oui"
NOUVEAU_EMAIL="4765982-oui@users.noreply.gitlab.com"

if [ "$GIT_COMMITTER_EMAIL" = "$ANCIEN_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$NOUVEAU_PSEUDO"
    export GIT_COMMITTER_EMAIL="$NOUVEAU_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$ANCIEN_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$NOUVEAU_PSEUDO"
    export GIT_AUTHOR_EMAIL="$NOUVEAU_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags

